# SciWi 1
__aanwezig__: Robin, Ruben, Vincent, Tibor, Arne VDK, Reine, Gert
10:33 Margo
__geopend__: 10:23

## 1. Goedkeuring Agenda
goedkgekeurd, goedkeuring verslag geschrapt

## 3. Planning vergaderingen

  ### 3.1 AV en AKV
  sowieso eerste vergadering AV en AKV voor verkiezen

  waarnemers bekrachtigen

  kringvergaderingen navragen

  misschien vroeger voor logo's door te geven aan Stura e.d.

  als eerste zondag kringen zelf willen vergaderen,

  wss 30e eerste AV/AKV


  ### 3.2 OOR
  OOR nog geen vast vergadermoment.

  zal week van stura veragderingen zijn

  woensdag of donderdag avond, week van stura AV's, nog geen dag beslist, om de twee weken

  vergaderingen moeten in kalender gezet worden

  ### 3.3 CuDi

## Update galabal
liefst 22e maart


al rondgegaan voor zalen

zaal in herent, geen zaalprijs, mss te klein

wij willen dit jaar geen sportoase, geen eigen hapjes, twee security moeten aanwezig zijn

de hoorn lijkt beste optie prijs-kwaliteit, heel de avond eigen cava en wijn drukt prijs, drankprijzen doorsturen,
weekprijzen verschillen van weekendprijzen, 1700 € voor 300 man, extra zaal van 600 € voor 500 man, is wel mooiere zaalprijs, geen cola maar pepsi, bijvoorbeeld bonnekes voor € 2, hapjes mogen niet zelf voorzien worden, ook geen chips ofzo, stewarts moeten aanwezig zijn, twee secutiry moeten aanwezig zijn

 vorig jaar was er +- 360 man, dus of echt goede reclame maken en voor 500 man gaan, of voor 300 man gaan, maar niet tussen de twee opties in. 300 man lijkt krap

salon georges klein en duur

musicafé is ook gemaild, geen kraantjeswater

voor vrijdag 14 bekrachtiging galabalteam via mail of FB

opendeurdag faculteit is op 23e maart

zo snel mogelijk naar kringen communiceren zodat optie genomen kan worden

## 4. Update jaarbegroting en jaarrekening Scientica Kringraad, CuDi en OOR

AKV, Cudi na herexamens

### Cudi

update financiën cudi na september

nog geen grote uitgaven




## 5. Documentatie

een aantal spontane structuren voor overdragen Documentatie

samenzetten

nextcloud goedgekeurd


## 6. Communicatie Scientica

  ### 6.1 Intern
  kluwen van communicatie

  workplace is een mogelijkheid, slack, facebook, mailinglijsten

  willen duidelijkheid wat gecommuniceerd wordt met welk kanaal, en wat is de optimale configuratie van kanalen

  facebook zoals nu lijkt minst optimale oplossing

  agenda's en verslagen blijven via mailinglijsten

  nextcloud misschien iets te hoogdrempelig en kan splitsing van communicatie veroorzaken

  workplace +: lage drempel, iedereen zit op facebook, alle groepen kunnen open zijn, zodat er cross-interactie kan zijn, meldingen komen apart, bundeling van scientica relevante dingen, er moeten geen dingen uit de persoonlijke facebook gefilterd worden.

  workplace -: nog altijd nieuw systeem, geen hiërarchie, wel bestanden maar ongeorganiseerd, privacy

  workplace voorstel: alles van agenda's en verslagen via mail, al de rest via workplace, geen facebook behalve extern

  facebook: één grote groep en apparte groepjes

  of één grote groep met hashtags: - mss afschrikwekkend om alles binnen te krijgen dat niet relevant is en soms te veel of te weinig inbreng hebben

  mail en comm binnen post zoals ze zelf willen

  OOR is voor belangrijke dingen via mail en dan meer casual dingen via facebook

  voorstel Vinny: zo veel mogelijk via mail, links van documenten op cloud doorsturen zodat bestanden, shiftenlijsten gedeeld zijn, dringende dingen via fb

  meer transparantie, kan mss opgelost worden door nextcloud

  CONCLUSIE: transparantie oplossen met nextcloud, geen groot nieuw platform, documenten, shiftenlijsten delen via nextcloud, zo veel mogelijk via mail, dringende dingen voorlopig via fb

  ### 6.2 Extern

  fb

  website:  oude dingen er af halen, hoe gaan bv verslagen georganiseerd worden, portaal naar nextcloud, meer inhoud, nieuwe site

  websites van andere kringen naar AKV


## 7. Nieuw logo Scientica
  HIER KOMEN LOGO'S van messenger

  kleurtjes lijken mss wat te happy

  a lijkt niet mooi

  willen we een klein logo?

  Sc met cirkel is niet duidelijk genoeg

  maar kleiner logo zou wel handig zijn

  vooral vorm waar we over overeen willen komen

  de vorm van dit logo is acceptabel

  op welke manier gaan we dit voorleggen aan de AV

  agendapunt eerste AV 30 september, nog niet verspreiden voor stemming

  willen we de logo's al in de AKV groep gooien

  ja, maar nog niet verspreiden

  ### textiel

  iedereen die betrokken is bij scientica heeft recht op textiel, ook OOR bv. scientica-pull met OOR-medewerker of CuDi medewerker

  niet via stura want dan staat stura logo op alle scientica textiel, mss interessant voor OOR

  één bestelling met personalisatie of OOR via stura

  voorstel frakske

  op AV vragen of textiel ook voor presidiumleden is (aan wie bieden we het aan en wat wordt het)

  opties: t-shirt, pull en jas mss zelfs meerdere? best beperken tot één dingen

  voor uniformiteit: één stuk dat zo veel mogelijk mensen kunnen overdragen

  voor degelijk textiel: bijvoorbeeld een duurdere jas

  mss t-shirts voor uniformiteit en frakske er nog bij

  VOORLEGGEN AAN AKV:
  willen we simpel, goedkoop stuk dat niet gepersonaliseerd is

  willen we een gepersonaliseerd stuk voor iedereen


## 8. Opvullen openstaande mandaten

  er is een doc met mandaat openingen

  cudi medewerkers moeten nog opgevraagd worden, maar komt wel goed

  openstaande mandaten op facebook pagina: Reine

  OOR en Cudi geven openstaande mandaten door

## 9. Updates

  ### 9.1 Bestuur

  ### 9.2 Kringraad
  Kalender, proberen zo snel mogelijk af te krijgen, nieuw communicatie en documentatiesysteem, galabal en cantusweek zijn bezig, alle kringen zijn in vergelijking met vorig jaar sneller met zalen rnzo dus scientica moet ook wat sneller op de bal spelen

  ### 9.3 CuDi
  grote bulbestellingen al een tijdje geleden gebeurd, printer gaat aangekocht worden, samengezeten ivm teambuilding, cudi als uitleendienst en tweedehandsboeken zien ze niet zitten, kaartenverkoop is ok maar niet in eerste week, schilden/logo's ophangen in cudi, kringboekjes is ok, payqonic tweede semester, wss geen kladblokken met scientica logo, inruimweek is nog een probleem omdat de leverdata nog niet vastliggen
  ### 9.4 OOR
  mss draaiboeken opstellen, hebben vergaderd over grotere dossiers, samengezeten met stura, was blij over verkiezingsstructuur, HR is zo goed als af, moet nog gestemd worden, enkel nog stemverdeling in faculteitsraad, meegeven aan elke kring hoe dit gaat gebeuren

## 10. Brainstorm

* GDPR: Ticketverkoop online voor galabal en akkoord gaan, en gelijk gecentraliseerde galabal
* OOR evenement: onderwijscafé van wina voor andere kringen, moet besproken worden met wina en binnen OOR, budget van Stura, daarna op AV brengen voor hulp van kringen
* Examenwiki scienticabreed
* vakkenverdeling voor krivers moet herbekeken worden, cudi zit samen met krivers
* sleutels van scientica lokaal, wie heeft die nodig, mss een voor alle verantwoordelijken, niet te veel (dubbele boekingen, vuil), 3 sleutels, één voor bestuur, één voor OOR, één voor Cudi, goedkeuren op AV

## 11. Varia
* Reine: heeft al twee viskaarten, pik viskaart van lennert als je hem ziet, bankoverdracht is gebeurd, communiceer met voorgangers wat er nog moet gebeuren, kaarten van vorig jaar moeten nog geschrapt worden
* Margo: wilt wisselbekers in de Hoorn, mss lenen bij ABInbev Reine: mss uitlenen bij Alma2, mss uitlenen bij provincie, vincent: druk zetten om glazen te gebruiken (Hoorn werkt niet met glazen), meenemen naar AKV

# TODO

arne: mailinglijsten, nextcloud tegen eerste AV

galabal: werkt voorlopige begroting uit voor de Hoorn

karel voorziet geld voor oor om te trakteren na vergaderingen, OOR kijkt eerst bij stura voor budget

Tibor: dankuwel om hier aanwezig te zijn en tijd vrij te maken, interessant en productief, veelbelovend jaar, laten we er een interessant jaar van maken
 
