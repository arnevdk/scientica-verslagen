# AV 1
zondag 30 september

__aanwezig:__ Oberon, raf, karim, robin, ruben, michael, gert, maite, jille, kristof seth, alexander, zoe, Jonas, eline, nathalie, clark, theresa, karel, reine, tibor, arne vdk,
(21:22) arne jansen, quinten

__afwezig:__

__geopend:__ 21:13

## 1. Goedkeuring agenda
goedgekeurd

## 2. Goedkeuring verslag SciWi
goedgekeurd op schrijffouten na, worden doorgestuurd

## 3. Bekrachtiging adviseurs SciWi

### 3.1. AKV - Vincent Verswijvel
ok

### 3.2. Cudi - Teresa Welke
ok

### 3.3. OOR - Thibault Deneus
ok

## 4. Bekrachtiging bestuur OOR

###  4.1. Voorzitter - Robin Kelchtermans
ok

###  4.2. Ondervoorzitter - Ruben Van Laer
ok

## 5. Jaarbegroting
actuals van jonas zijn binnen
geen presentatie van Jaarbegroting
er zal meer geld gaan naar OOR
jaarbegroting zal gepost worden wanneer klaar

### Cudi
nog geen begroting


## 6. Nieuw Logo
### groot logo
er is nog een streepje aan de A aangepast
het logo wordt in huidige staat voorgesteld
motivatie voor het nieuwe logo is vanwege scientica rebranding en oud is lelijk, minstens twee kringen ondersteboven, reeds lange tijd een plan
stemming:

voor: 15
tegen: 0
onthouding: 0
unaniem goedgekeurd

dit logo zal vanaf nu gebruikt worden, dit gaat ook voor de cursussen van volgend semester cudi, webteam

### logos voor geledingen
oor: misschien uitschrijven als overkoepelende onderwijsraad of scientica onderwijs

### klein logo
klein logo kan handig zijn voor social media, cudi, lidkaarten
waarom klein logo er nog bij? het is een logo dat mag gebruikt worden voor kleinere formats, groot logo zal waarschijnlijk niet leesbaar zijn als het verkleind wordt

moet er een klein logo bijkomen?
stemming

voor: 13
tegen: 2
onthouding: 0

welk klein logo?



opties:
  1. lijntjes van cirkels iets langer, ientica weg
     de i kan ook gedraaid worden, sci verwijst mss meer naar wetenschappen, vijfkante symmetrie lijkt op fossiel
  2. gewoon entica weg
     mensen vinden sci loucher dan sc, dit is duidelijk genoeg, in de codex wordt sc. gebruikt als afkorting voor wetenschappen

aan de twee opties is wel als nadeel dat de kringen een beetje wegvallen     

tegen volgende AV zullen er nieuwe voorstellen zijn wegens gebrek aan consensus

kleurtjes zijn vrij in te vullen door de kringen, logo mag ook licht gecustomized worden
nut wordt betwijfeld, kleur kan aangepast worden aan achtergrond, argument wordt aangehaald dat door de kring zelf het logo weinig gebruikt wordt, het kan nog steeds toegepast worden als de toepassingen weinig zijn

waarom zou het logo van een overkoepelende organisatie aangepast worden, eigen logo kan gebruikt worden voor customization

voor uniformiteit: 13
tegen uniformiteit: 1
onthouding: 1

één uniform logo met kleurtjes
hebben officieel geen mening of symboliek, maar kleur kan het wat vrolijker maken
geos wilt geen bruin, kleuren misschien ordenen op alfabet van kring, yay kleurtjes, er kan ook gewerkt worden met een randje rond de streepjes voor minder harde kleurtjes, chemika vindt roze te vrolijk, er kan misschien nog gewerkt worden aan de volgorde, de tekst kan in één kleur en de kringetjes kunnen in een andere kleur hiervoor is een voorstel met donkerblauwe kring

willen we een logo met kleur?
voor: 10
tegen: 1
onthouding: 4

willen we twee kleuren of één kleur per kring?
5 kleuren is misschien te vrolijk, twee kleuren zijn iets eleganter/strakker
er wordt gevraagd om de keuze te verdagen en op meer uitgewerkte voorstellen te wachten, maar deze mening wordt niet gedeeld

wordt de beslissing verdaagd?
voor verdagen: 10
tegen verdagen: 1
onthouding: 5

er wordt aangehaald dat er nadat er op facebook voorstellen zijn geweest en discussiemogelijkheid was, dat het jammer gevonden wordt dat er nu nog zoveel gediscussieerd wordt.

Er worden tegen volgende AV voorstellen met logo's uitgewerkt
input over kleuren mag naar voorzitter@scientica.be gestuurd worden

## 7. Intern en extern communicatiebeleid
het advies van SciWi wordt voorgesteld (zie SciWi verslag)

### Documentatie
Nextcloud wordt voorgesteld
voor: 16
tegen: 0
onthouding: 0

unaniem

### communicatie
email aanpassen? tutorial voor persoonlijke instellingen posten

momenteel zijn de facebookgroepen:
* mandaten
* OOR
* gesprek Cudi
* gesprek SciWi
* oorgesprekken
* Scientica presides gesprek
* Scientica presides groep
* Scientica bestuursgroep

Er moet gesnoeid worden in groepen, voor elke geleding een groep met hashtags of aanduidingen
het kan wel handig zijn als er met weinig iets besproken moet worden om in een kleine groep te werken
daarom mailadressen per post

fb groepen zijn nuttig om snel antwoord te krijgen en voor groepscommunicatie en discussies, maar zoeken is moeilijk
sowieso veel spam

fb enkel voor discussies, polls, snelle mededelingen
mails voor mededelingen

er wordt aangemoedigd om wel actief met mail bezig te zijn, rekening houden met één dag speling




## 8. Updates

### 8.1. Cudi
voorstellen sciwi:
* payconiq is nagevraagd geweest, vroeger nuttig omdat er een voordelige actie was, het is nu niet meer zo voordelig.
* schilden moeten gemaakt worden door de kringen, cudi doet dit niet zelf, niet persé in hout, stickers op de deur kan ook
* acco kringkaarten zijn nog niet verkregen van Acco, mss kan die van vorig jaar nog gebruikt worden

### 8.2. OOR
* er zijn nog vrije mandaten: 1 diversiteit, 3 fpoc, 3, waarnemers
* HR is bijna af
* focus op communicatie en bekendheid, campagne daarvoor

## 9. Varia
* Karel: de vorm van het schild staat in de codex, er is een houtatelier in leuven op de ring waar het chemikaschild is gemaakt, ik ben blij dat ik het logo in mijn emailhandtekening kan zetten
* Teresa: komt SciWi van samsin en gewerkt
* Siene: sta ik in de mailinglijst oor ik: ja, donderdag 3 oktober alle tafels uit het lokaal voor chemika
* Reine: als je een sleutel nodig hebt: ik heb email gsm whatsapp, maar niet op fb; aan bios: allerlaatste kans als de vuurschaal niet bij mij weggehaald wordt is hij van mij

__gesloten:__ 22:54
