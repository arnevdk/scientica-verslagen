# AV 2

14/10

__geopend:__ 21:20
aanwezig: arne vdk, arnej, raf, karim, oberon, gert, teresa, maité, kristof, alexander, chemika rando 1/gilles, zoë, robin, ruben, michaël, clark, jonas, tibor (21:30) quinten, (21:43) reine, (21:46) jille
, (21:47) eline
## 1. Goedkeuring agenda

logo en deelbegroting cudi worden verdaagd

goedkeuring aanmaken derde sleutel wordt toegevoegd

wina wilt graag op een volgende AV stemverdeling op AV bespreken

de agenda wordt goedgekeurd

## 2. Goedkeuring verslag

het verslag wordt goedgekeurd op shrijffouten na. teresa wilt haar varium aanpassen




## 3. Jaarrekening (Zie bijlage)

Jonas stelt jaarrekeningn 17-18 voor

goede internationaalweek vanwege samenwerking kringen, orientation days mooi binnen begroting, vlot gelopen, winst internationaal

rode activiteiten zijn niet doorgegaan

scienticafe slechter dan verwacht, vooral derde -125 eur. geen logische verklaring gevonden buiten samenloop van omstandigheden (minder opkomst en minder gedronken) gratis vat van laatste scienticafe verzet naar bbq.

weinig over activiteiten te zeggen. eetstandje zomercursus normaal; weekend zoals verwacht break even; plan sjarel goed tov begroting; skireissponsoring is er gekomen, skireis heeft meer geld gekost vanwege extra bed dat aangekocht moest worden om sponsoring te krijgen, ook iets meer uitgegeven aan skireis promo acties; galabal draait altijd hard negatief, maar nog minder dan gehoopt; massacantus heeft boven begroting gedraaid; jobfair actuals zijn nu binnen en die worden deze week uitbetaald vanwege bedrijven die niet wilden betalen en communicatie met jobfairverantwoordelijke. uitgaven van jobfair zijn vanwege uitbetaling aan kringen; scienticantus minder dan normaal maar verwacht vanwege lage opkomst; cantusweek goed; scienticup en bbq lijkt op heel veel uitgaven begroot maar is logisch vanwege overdracht gratis vat; debat is niet doorgegaan; teambuilding goed; internationaalcantus is niet doorgegaan, andere cantus van internationaal zit bij in cantusweek; lan-party was niet begroot;

textiel is niet gebeurd; werkings en vzw subsidies van loko zijn hetzelfde zoals altijd; staatsblad terugbetaald door LOKO; verzekering cudi is onderling geregeld met cudi; administratieve kosten zijn bankkosten, dit is een normale kost; guidogidsen draaien zoals elk jaar op nul; kringensteun is binnengekomen; OOR en COBRA wordt terugbedtaald door faculteit; cudi staat nog in het rood omdat de cudi een semester later uitbetaald wordt; onvoorziene kosten worden misschien als buffer genoteerd, jonas overlegt met karel; om de uiteindelijke jaarrekening op nul te laten uitkomen wordt naar het totaal gekeken en de buffer wordt opgevuld tot we op nul draaien. dat bedrag wordt dan uit de kas gehaald om het te laten uitkomen, er is nog ongeveer 4000 - 5000 euro buffer, daar moet nog geld voor binnenkomen

galabal en onvoorziene kosten worden anders gedaan bij begroting

jaarrekening wordt goedgekeurd door AV en wordt neergelegd

kringensteun en buffer scientica zijn fout, worden nagekeken


## 4. Begroting (Zie bijlagen)
### a. Algemene begroting


Tibor stelt jaarbegroting voor.

### internationaal
internationaal is nog een vraagteken omdat er nog niet geweten is of alle activiteiten doorgaan; etentje internationals was vorig jaar gepland maar niet georganiseerd, dus begroting moet nu nog bekeken worden. idee is nog niet concreet.

### scienticafé
zijn voorzichtig begroot op -75 euro verlies elk. we zullen er wel maar 4 doen hoewel er 5 begroot staan. er moet er nog één uitgehaald worden; er is geen gratis vat inbegroot, dit moet nog inbegroot worden bij of een scienticafé of de bbq; aan de scienticafé verantwoordelijke moet doorgecommuniceerd worden dat die 75 euro niet uit te geven aan bv promo is maar om de rekening van VTK te betalen en op break even uit te draaien. dit mag dus iets minder maar we wachten er liever mee tot na de eerste scienticafé om een schatting te kunnen maken.  er kan nog extra inbegroot worden voor promo/inkleding maar is moeilijk om op voorhand te doen.

### activiteiten
nog geen actuals vann zomercursuseetstand, wss verlies, skireis sponsoring is verminderd, 1200 potentiële sponsoring, yanice heeft vorig jaar veel op promo inbezet, nu willen we daarop besparen en zetten we dit op -150 ipv -220; deze -220 was wel met extra bed dus eigenlijk heeft promo -100 gedraaid en mag de huidige nog lager als we willen besparen, kan evt nog buffer van één bed in maar contract is kleiner dus bed moet mss niet inbegroot worden; galabal staat momenteel op -1500 inkomsten en uitgaven zijn op dit moment moeilijk te schatten omdat er nog niet veel concreet geweten is ivm regelingen met eten, drank en zaal, vanwege meerdere lustra wilt wina budget optrekken eventueel met extra geld van bepaalde kringen,  dit kan met vrijwillige inleg van kringen of algemene verhoging van budget. we wachten hier best nog wel af voor onderhandelingen. wordt verdaagd tot herbegroting; massacantus is een beetje meer maar mag een beetje buffer in zitten, iets meer rekening houden met vorige jaren omdat vorig jaar een uitzonderlijk goed jaar was; jobfair staat nog op 0 omdat we niet weten welke bedrijven aangetrokken gaan worden; scienticantus redelijk realistisch begroot, niet te voorzicthig; cantusweek op minder verlies begroot omdat we extra korting van medica gekregen hebben; cantusweek moet concreter begroot worden; debat is nog niet begroot omdat er nog niet geweten is of dit georganiseerd zal worden, dit zal waarschijnlijk gesponsord of terugbetaald worden bij faculteit of loko; lan party op 0 vanwege onzekerheid over of het doorgaat of niet; après av is normaal kostenloos omdat loko alles terugbedtaald; vat van faculteit voor helpen met infodagen wordt gegeven op BBQ, de winst van dit vat op de bbq kan ergens anders in de begroting gestoken;

### algemene werking
textiel voorlopig op 0 omdat we nog niet weten of scientica het textiel sponsort, momenteel is dit niet het geval; werkingssubsidies omhoog vanwege hogere kosten staatsblad; verzekeringen cudi moeten op 0 staan; buffer is nu lager vanwege hogere kosten vorig galabal; kringensteun lijkt een copy-paste foutje in actuals van oor/cobra, kringensteun zal wel verhoogd worden; actuals cudi komen oktober/november, maar zijn wel kosten die binnen zulle komen dus moeten wel begroot worden; dit jaar geen cobra, oor budget ging verhoogd worden maar lijkt laag nu; cobra budget moet overlegd worden bij faculteit; onvoorziene inkomsten staan op 100 waarvan al 50 euro gebruikt is. dit zal waarschijnlijk dus opgetrokken worden. Onvoorziene kost lijkt heel weinig tov omzet, wina begroot 10x zo veel in. volgens loko 10% op variabele uitgaven + mss extra voor bijvoorbeeld boetes of iets kapotmaken; buffer scientica staat er niet in;

momenteel draait de jaarbegroting zeer negatief, deze zou op nul moeten uitkomen. nu staat er vaak het saldo als inkomst of uitgave. momenteel lijkt het eerder een opsomming van getallen en geen begroting. een begroting moet als richtlijn dienen een een goed overzicht geven van de totale omzet;


geld van jobfair komt pas als geld van guidogidsen van kringen binnen is, karel stuurt naar betreffende kringen een mail met bedrag

opmerking ik structuur begroting, evt voor herbegroting

buffer van scientica in de begroting zetten betekent het uiteindelijke saldo op 0 te laten draaien door daar te krediteren of debiteren; als we over hebben kan dit in de begroting gestoken worden als dit over 4000 euro gaat gaat de rest naar de kringen; principiëel zou een begroting dus eigenlijk zonder buffer in gedachten gehouden opgesteld worden zodat de begroting op zich clean is en op nul draait, en zou de buffer er dus niet ingezet moeten worden, er moet dus gekeken worden of de buffer er niet uigehaald kan worden en de begroting zo op nul kan draaien

### b. Deelbegroting OOR.

sturav is nieuw en is wat discussie rond, dit staat er voorlopig niet in; kleine kost voor herbruikbare bekers op ooroverleg; flessen drank is een schatting; oor trakteert wordt geschat op 84 consumpties * 1.5 eur; overschot van oor overleg kan evt op sturav;

OOR wordt bedankt voor eerste keer begroting te maken door bestuur; begroting kan overzichtelijker vanwege parameters enzo; oor trakteert is consumptie en op vergaderingen is voor fles/water/cola

1eur67 is voor 6 bekers;

de deelbegroting wordt goedgekeurd
## 5. Sleutels
dit moet aangevraagd worden omdat dit een lokaal van de campus is. zodat reine en tibor er altijd een hebben en er een kan uitgeleend worden. dit wordt goedgekeurd door de vergadering

## 6. Updates
### a. Nextcloud als documentatieplatform
werkt arne legt uit
### b. Cudi
cudi loopt vrij goed, kringkaarten acco zijn ontvangen, deze keer zonder jaartallen dus mss voor meerdere jaren. logo's in de cudi zijn bekeken, schilden zijn veel te groot maar er worden twee stickers per kring gevraagd om op de deur te plakken, er kunnen ook grotere stickers gemaakt worden voor op deur van cudi en scienticalokaal
### c. OOR
plaats vrijgekomen voor facultaire werkgroep duurzaamheid, like ons op facebook, insta en twitter
## 7. Uitnodigingen

### a. Scienticaweekend 2-4/11
tibor nodigt uit, zal doorgaan in chiro ESJEEWEE sint-joris-winge

## 8. Varia

* arnej: vraagt iets langere pauze om post van skireis te gaan halen
* raf: scienticalokaal sleutel moet sneller teruggegeven worden, reine: ik heb in mijn peignoir sleutels moeten fixen
* karim: maite: zeg fann ipv fannE
* alexander: hoopt me derde sleutel dat we binnenkunnn op altijd
* reine: chemika: quinten zijn porno niet op nextcloud, ook niet van jille; ik ben naar hier gelopen, degene die het dichtst erbij zit winst
  raf: 7.52, karim: 10km 365m 73 cm 3580960 baartsec + def baartsec, oberon: 9.4; gert: 14: teresa: 8.6; maite: 9.2 rando: 15; quinten 4.8, jille: 7.9, kristof: pi; frederic: 5.666, alexander 5.345, zoe 7.2 aderik: 6.2 robin 5.2 ruben 6.9 michale 7.3 clark 3.5, was 3.7
  karim: sossen zijn aan de macht in leuven dus deelbegroting
* zoe: er staat wel een jaartal op de acco kringkaarten
* tibor: hoopt vroeger te kunnen starten met vergadering, wilt sleutel bijhouden om op tijd te kunnen starten, volgende keer vroeger aankomen

__gesloten:__ 23:00
