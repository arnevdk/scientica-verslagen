# AKV2
 14/10

__aanwezigheid:__ raf, karim, arnej, gilles, quinten, jille, kristof, eline, frederic (tot 00:15), alexander, zoe, adje, clark (tot 23:30), reine, tibor, arne vdk ()

 __geopend__:

## 1. Goedkeuring agenda
de agenda wordt goedgekeurd
## 2. Goedkeuring verslag
vragen en discussies wel opnemen in verslag
wordt goedgekeurd op doorgestuurde opmerkingen na
## 3. Prijzen van lidkaarten/cantussen/. . . binnen Scientica (Zie bijlage)
geos wilt aanhalen dat de prijzen van lidkaarten, guidogidsen, cantussen, ... nogal laag liggen bij ons en dat we misschien het kunnen optrekken, en of we die gelijk willen trekken binnen scientica.

cantusprijs optrekken omdat iedereen het doet is geen goede reden, maar het lukt bij sommige kringen wel om het te houden en bij anderen niet.

vorig jaar bij wina is cantusprijs hoger gezet in het begin van jaar naar 12 euro.

uniformiteit kan gemotiveerd worden vanuit het feit dat lid worden van één kring voordelen kan opleveren bij andere scienticakringen en dus ongelijke prijzen voor relatief meer of minder voordeel kunnen zorgen. dit gaat over kleine bedragen dus is niet zo relevant

prijzen willen uniform binnen scienticafe

vtk heeft prijs opgetrokken vanwege zaal, chemika doet daardoor ook 11/13 en minder lukt niet, maar er zijn nog kringen die 10/12 doen

er is echter wel competitie in de verkoop van guidogidsen wegens prijsverschillen bij veel kringen overgenomen van vorig jaar

merkator wou niet te duur verkopen vanwege schrik dat het niet verkocht geraakt 3.90

prijzen voor guidogidsen afspreken begint in campagne, prijs is afhankelijk van aantal besteld. daarom kunnen we met sientica samen bestellen en één prijs vastleggen en zo verder verkopen aan een prijs met al dan niet winst. Er kan bepaald worden of we allemaal hier winst op willen maken of break even draaien

het is niet altijd duidelijk welke impact een prijsverandering voor guidogidsen op elke kring

guidogidsen zijn dit jaar verkocht van scientica aan de kringen aan

sommige kringen hebben over, sommige zijn te kort

het lijkt de bedoeling om winst te maken op guidogidsen, maar veel kringen draaien verlies daarop en doen het dus als geste aan hun leden.

er mag sowieso een beetje winst gemaakt worden om de niet verkochte te compenseren

als één kring eruitstapt heeft de rest wel een duurdere prijs en als je dan verlies draait dwing je ook andere kringen verlies te laten draaien

als we beslissen dat elke kring zelf prijs mag bepalen moet er wel rekening gehhouden worden met een eerlijke prijs zodat andere kringen hun gidsen ook wegkrijgen

daarom is het misschien relevanter om toch een prijs te zetten voor guidogidsen, en ook voor lidkaarten bijvoorbeeld ook vanwege cantussen en cudi

indien de prijs voor lidkaarten gelijkgetrokken wordt, moet er wel beslist worden in welke richting 8,9 of 10

er moet wel een goede motivatie zijn voor een prijsverhoging, want mensen gaan uittellen waarvoor ze eigenlijk betalen, en oudere mensen gebruiken minder van de voordelen die kringen meestal bieden met hun lidkaarten

de verhoging is waarschijnlijk een grotere afschrikking dan de hogere prijs zelf

scienticakringen zijn eigenlijk verplicht om scienticaleden dezelfde voordelen te bieden dus dan is een gelijke prijs voor een lidkaart wel aangewezen

er zijn ook een aantal kleine voordelen zoals gratis lustrum dingen maar die heffen elkaar op tov de kringensteun

er wordt een voorkeursstemming zonder bindende beslissing gehouden over het principiele idee van gelijktrekking
voor:11
tegen:1
onthouding: 11
staking der stemmen maar geen herstemming aangezien het om een peiling ging

## 4. Samenwerking met Guido
hierboven
## 5. Aankomende activiteiten
### a. Infomoment Skireis 15/10
slecht moment voor arnej maar komt wel op geplande datum in scienticalokaal er wordt een mail rondgestuurd met info; er moet zeker voldoende man uitgenodigd worden op het infomotent-event op fb. we hebben 75 posters, nu worden er 20 verdeeld, kringen moeten deze ophangen, probeer er zo veel mogelijk te doen. arnej zal ook in het centrum zo veel mogelijk posters proberen te plakken. kunnen ook op TD's geplakt worden. indien een kring meer posters wilt plakken, contacteer arnej. er zijn al inschrijvingen voor de skireis
### b. Scienticafé 15/10
er is een promotekstje en een shiftenlijst: bijna volledig ingevuld. bios moet ook nog een shift fixen. promo wordt rum-cola aan €2 (normaal 3 in elixir), wij voorzien 3 flessen bruine rum. hier halen wij de inkomsten terug uit. er wordt nog versiering gefixt. maak nog genoeg reclame
### c. Scienticaweekend 2-4/11
is er een voorkeur om avondeten te voorzien op de vrijdag van het weekend, want andere jaren is de aankomst op 19u; indien geen avondeten zullen mensen waarschijnlijk later aankomen,
frietjes bestellen met volk aanwezig
verhuren en uitlenen van materiaal kan via materiaal@wina.be
tibor heeft een idee voor een nachtspel
er moeten zowel buiten als binnenactiviteiten voorzien worden
gebouw is vastgelegd, contract is doorgestuurd
eten zelfde als vorig jaar maar bbq ipv spaghetti omdat er budget vrijkomt
goed nadenken over de logistiek van bbq, je vind wel iemand die met de auto komt die meegaat
inschrijvingen komen morgen online, inschrijvingen zo snel mogelijk, kunnen op website, wordt doorgegeven aan webteam

weekend enkel voor presidium of ook voor scienticaleden
hangt af van hoeveel man er binnen mag: 35
dit is te klein om nu ook leden toe te laten, maar er kan volgend jaar naar een grotere locatie gekeken worden
mss vreemde activiteit voor leden die niet direct link hebben, maar bijvoorbeeld vaste medewerkers mogen wel. voorrang geven aan scientica presidium
locatie is 50 euro duurder geworden dan vorig jaar

### d. Massacantus 6/11
zie mail, kaarten over regelen via centrale organisatie niet zelf. kaartenverkoop op één moment tegelijk maar niet tesamen op één locatie: eerst eigen leden, dan deelnemende kringen (niet scienticaleden daartussen)

op zich is het niet nodig om indien er op het eerste verkoopmoment enkel aan leden verkocht wordt om die verkoopsmomenten samenvallen

het is structureel wel makkelijker te organiseren indien er na één dag waarop iedereen zijn verkoopmoment houdt met de organisatie een regeling uit te werken om de overschot te verkopen


vtk krijgt meer procent omdat zij logistiek meer organisatie doen

elk jaar stewardjassen en kannen te kort, kringen moeten die verzamelen en voorzien, maak desnoods iemand verantwoordelijk om die naar blok 6 te doen. probleem: hier verdwijnt materiaal en dat wordt vergoedt, maar wel vervelend. daarom zou het beter zijn dat massacantus bij loko kannen gaat halen. dit wordt dan meegenomen naar het overleg

kom naar je shift want boete

shift je op het gegven uur of wanneer de bedoeling voor je shift is. dit moet gezamelijk afgesproken worden . zal aangehaald worden op overleg

kaarten:
bios: 25
chemika: 31
merkator 18
geos: 11
wina: 55

er worden vaten gepikt op de massacantus, ook al zijn er shifters. hier moet beter op toegezien worden. meenemen naar overleg
### e. Networking Event Faculteit Wetenschappen 6/11
schrijf jezelf of je vertegenwoordiger of pr verantwoordelijke in voor networking event, deadline 22/10. er moet van elke kring iemand zijn, liefst met PR
## 6. Updates
### a. Kalender
scienticafé nummer 4 staat momenteel nog niet vast. gezet op 11 maart. adje moet regelen met VTK indien dit niet lukt 18 maart, hiervoor kijkt chemika na of ze dit kunnen fixen met proffentap

#### >> Lezing Intellectueel Eigendom

#### >> Après-AV
wij moeten enkel shiften voorzien, loko regelt zaal enzo. loko stelt OHL voor, wij vinden albatros een meer vertrouwde optie. kan ook vanachter in de maxim'o (key-club) met pintjes aan 1 euro of 1.10 en hapjes en geen steward

7 december, dan is het wel koud op terras van key club
### b. Mandatenwerving
deze zijn nog niet door alle kringen openbaar gespamd (wina en bios wel). dit moet zeker nog gebeuren


### 7. Varia
* arne: zet uw kalenders in Nextcloud
* arnej: zijn er kringen bezig met mindmates project? wij zijn me 10 man maar met 20 man in totaal kan je een soort sessie krijgen, pols eens bij je presidium of hier nog volk geinteresseerd is en contacteer vicepreses@wina.be en dan regel ik da karim: facultaire werkgroep diversiteit spoort dit ook aan; ehbo-cursussen vanuit scientica; willen kringen ehbo-vorming arnevdk fixt vanuit scientica
*  raf: ridouani wordt burgemeester van leuven
* papa van kompany eerste zwarte burgemeester
* aderik: sorry dat ik geen kalenders en shiftenlijsten kan lezen, ik wil een svg logo
* reine: aderik: het is niet ergens
* Tibor: sorry voor lange vergadering, kom eerder

 __gesloten__:
