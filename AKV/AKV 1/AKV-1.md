# AKV 1
zondag 30 september 2018

__aanwezig:__ Arne Jansen, Oberon, Karim, Aderik, Siene, Quinten, Jille, Kristof, Seth, Alexander, Zoe, Lucas, Frederik, Nathalie (tot00:11), Esmée, Clark, Karel, Margo

__afwezig:__

__geopend:__ 23:02

## 1. Goedkeuring agenda
samenwerking guido
opm varia toevoegen en activiteit updates bij activiteiten: opkomende activiteiten hebben effectief progress, de rest nog niet
## 2. Verkiezingen
### 2.1. Weekend - Frederic Guillemins
stelt zich voor
verlaat de ruimte: 23:06
vragen:
* vorig weekend was de inhoud van activiteiten in het weekend is vrij laag, hoe ga je dit aanpakken, algemeen: waarop moet volgens jou de focus van het weekend liggen
* hoeveel cara pilsjes ga je drinken op weekend
* hoe ga je ervoor zorgen dat er altijd iemand nuchter blijft
* hoe ga je ervoor zorgen dat de begroting effectief wordt uitgevoerd tijdens het Weekend
binnen: 23:11
buiten: 23:17
binnen: 23:21
stemming
voor: 5
tegen: 0
onthouding: 0
unaniem

advies: todo: nog iemand zoeken om weekend te doen, steun van kringen vragen, kan ook rekenen op bestuur voor ondersteuning, er mag zeker gedelegeerd worden, lanceer een vraag voor bv een spel


### 2.2. Internationaal - Alexander Clark & Nathalie Cenens (team)
buiten : 23:23
* strategie om internationale studenten bij scientica te betrekken
* noem de buurlanden van tadzjikistan
* hoe zie je de samenwerking met de internationaalwerkgroepen binnen de kringen
binnen: 23:25
buiten: 23:31
binnen: 23:32

voor: 5
tegen: 0
onthouding: 0

### 2.3. Galabal - Margo Witters & Esmée Verkooijen (team)
buiten:
* hoe willen jullie de gala in galabal invullen
* hoe ga je de presesdans aanpaken
* hoe zijn jullie van plan jullie taken verder te verdelen; gaan jullie nog mensen ronselen
* wil je mijn date zijn
binnen: 23:36
buiten: 23:41
binnen: 23:42

voor: 5
tegen: 0
onthouding: 0

### 2.4. Plan Sjarel - Aderik Voorspoels
buiten: 23:42
* verduidelijk jou themakeuze
* wanneer plan je een plan sjarel, en waar?
* wil je mijn date zijn
binnen: 23:45
buiten: 23:48
binnen:

voor: 5
tegen: 0
onthouding: 0

## 3. Openstaande mandaten
* Jobfair, belangrijk dat we hier iemand voor vinden
  - bestuur heeft advertenties loko-style gemaakt
  - er wordt aan de kringen gevraagd om deze advertenties te verspreiden via sociale media en op vergaderingen mensen aangespoord
  - voor jobfair is het wel belangrijk dat het iemand bekwaam is
* Hulp voor Weekend
* lezing
* teambuilding

deel ook advertenties van OOR



## 4. Prijzen van lidkaarten/cantusprijzens/... binnen Scientica
verdaagd
## 5. Aankomende Activiteiten

### 5.1 Teambuilding
zal volgende week niet plaatsvinden vanwege gebrek aan verantwoordelijke
### 5.2. Scienticafé 1
er is langsgeweest bij VTK, contract verkregen. er is een prijsverandering tov vorig jaar. vaste kost van 75 euro is 150 euro geworden. alle winst onder 150 euro ging naar vtk, nu 50/50 verdeeld. budget volgt na jaarbegroting. tegen vrijdag volgen promotekstjes en grafisch materiaal. shiftenlijsten komen op termijn. eline wilt helpen met grafisch materiaal. thema wordt pirates of the scienticafé. budget is 75 verlies

Laatste scienticafé moet nog doorgegeven worden aan VTK. Deadline daarvoor is 15 oktober. De optie wordt doorgestuurd per mail naar AKV, en zal zo voor volgende AV vastgelegd worden

kringenzuip voor tweede scienticafé. niet voor eerste omdat dat toch al volk trekt

### 5.3. Infomoment Skireis/Skireis
arne zit morgen met skikot samen om alles te bespreken. er is nog iets mis met de inschrijvingslink. facebookevenement zal vanuit skikot georganiseerd worden, daarna zal ons evenement aangemaakt worden.
we gaan op skireis
## 6. Evaluatie Activiteiten
### 6.1. Scientica @ Orientation Days
algemeen goed, wina en chemika afwezig. jammer want wel veel internationale studenten van wina. wegens omstandigheden was wina internationaalteam niet beschikbaar, miscommunicatie. Belang van internationaal functie en orientation days wordt blijkbaar niet goed doorgecommuniceerd aan nieuw verkozen internationaalteams in de kringen. De shiften van loko waren wel opgevuld, maar internationaalteams worden verwacht om ook de andere verantwoordelijkheden op te nemen. Wina excuseert zich vanwege interne problemen in internationaalteam en probeert interne oplossing te zoeken.

Faculteit geeft complimenten. Internationalen vonden het leuk. Ongeveer 15 euro winst. ook 1 bak over.

stuur de activiteiten volgende weken door naar de internationaalverantwooredelijken en presides van kringen

contacteer meer presides bij problemen met internationaalteams

Wina aanwezigheid is heel belangrijk vanwege aantal studenten

## 7. Updates
### 7.1. Galabal
zaal: de hoorn, zie SciWi

### 5.2. Massacantus
veldje voor Alma 3 ligt vast, datum 6 november. wordt voornamelijk gedragen vanuit VTK. er wordt aan de kringen gevraagd om de feestverantwoordelijken erbij te betrekken indien zij dit willen. Massacantus valt samen met networking event van de faculteit. Per kring moeten er nuchtere bv PR verantwoordelijke vertegenwoordigen voor networking event.

nog geen duidelijke verwachtingen van feestmedewerkers voor massacantus, zou wel aangenaam zijn als er minstens 3 man van scientica is om ook organisatorisch engagement te tonen.

### 5.3. Lezing Intellectueel Eigendom
via wieter is er een aanbod gekomen om een lezing te doen over Intellectueel eigendom. we hebben onze interesse getoond. willen we dit doen. mail wordt verspreid (tibor). klinkt de moeite waard. weinig werk: aula reserveren, receptie achteraf

### 5.4 Cantusweek
Veel korting wegens presidiumprijzen bij medica: 90 euro per cantus.


### 5.3. Kringruimtes
er worden 6 lokalen ter beschikking gesteld in de G tegen 2022-2023. Moet aangehaald worden op presidesoverleg. deze dingen moeten zeker zwart op wit op papier staan inplaats van vaag informeel geregeld

zo veel mogelijk aanhalen op vergaderingen
### 5.4 Après AV
Fakbar openhouden na LOKO AV voor LOKO AV
goedgekeurd
Maxim'o kan, er worden misschien tappers voorzien
het heeft wel meerwaarde om zelf te tappen
datum staat ergens vast, liefst in ander semester als plan sjarel


## 6. Varia
* Arnej: proberen iets efficienter te vergaderen, ik heb morgen les fcking vroeg; tibor: is de eerste vergadering met grotere punten maar zullen er rekening mee openhouden
* raf: 1: drugsfestijn met kinderboerderij hans teeuwen roerbakei, heroine dealen op kinderboerderij 2: voorstel voor presidesdans (link van filmpje). Tibor: de voorzitter doet echt mee dan. raf: eigen mix met salsa
* karim distantieert zic van preses
* aderik danku voor verkiezeing en mag binnen op plan Sjarel
* jille eigenlijk heeft arne vandaag al les
* seth: dubbele dingen uit scienticalender aanhalen
* alexander: wanneer komt geld van jobfair en cudi van vorig jaar. bestuur vraagt na bij maité.
* frederic: wilt toegang tot begroting van vorig jaar. karel: vraag best aan voorganger; staat in drive. moet nog in mailinglijst
* Margo: wie wilt mijn date zijn
* Karel: nummers van bestuur op bord.
* Reine: frederic permissie geven om zaal vast te leggen, locatie van vorig jaar was wel goed



__gesloten:__
